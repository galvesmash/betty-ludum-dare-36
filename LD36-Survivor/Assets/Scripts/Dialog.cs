﻿using UnityEngine;
using System.Collections;

public class Dialog : MonoBehaviour {

    public SpriteRenderer[] technologiesImages;
    public GameObject technologies;
    public GameObject itens;

	public bool item11Status { get; set; }
	public bool item12Status { get; set; }

	public bool item21Status { get; set; }
	public bool item22Status { get; set; }

    public SpriteRenderer[] item1;
    public SpriteRenderer[] item2;
    public SpriteRenderer[] results;

	void Start(){
		item11Status = false;
		item12Status = false;
		item21Status = false;
		item22Status = false;
	}

    public void EnableTechnologie()
    {
        technologies.SetActive(true);
        itens.SetActive(false);
    }

    public void EnableItensTechnologie()
    {
        technologies.SetActive(false);
        itens.SetActive(true);
    }

    public void SetImageTechnologie(int[] techs)
    {
        for (int i = 0; i < techs.Length; i++)
        {
            technologiesImages[i].sprite = Resources.Load<Sprite>("Images/Technologies/Tech" + techs[i]);
            results[i].sprite = technologiesImages[i].sprite;
        }        
    }

    public void EnableDisableDialog(bool enabled)
    {
        this.gameObject.SetActive(enabled);
    }

    public void SetImageItem(int itemPos, int posArray, int item, bool shade/* = false*/)
    {
        switch (itemPos) {
		case 1:
			item1 [posArray].sprite = Resources.Load<Sprite> ("Images/Itens/Item" + item);   
                
//			if (posArray == 0)
//				item11Status = shade;
//			else
//				item12Status = shade;

            if (shade)         
				item1[posArray].color = Color.white;
			else
				item1[posArray].color = Color.black;
//			if (shade)
//				item1[posArray].color = new Color(0, 0, 0, 255);
            break;
        case 2:
            item2[posArray].sprite = Resources.Load<Sprite>("Images/Itens/Item" + item);

//			if (posArray == 0)
//				item21Status = shade;
//			else
//				item22Status = shade;

			if (shade)         
				item2[posArray].color = Color.white;
			else
				item2[posArray].color = Color.black;
//			if (shade)
//				item2[posArray].color = new Color(0, 0, 0, 255);
            break;           
        }           
    }

    public void ResetImages()
    {
        Sprite undefined = Resources.Load("Itens/Item" + Item.OBJECT_TYPE.UNDEFINED) as Sprite;

        for (int i = 0; i < technologiesImages.Length ; i++)
        {
            item1[i].sprite = undefined;
            item2[i].sprite = undefined;

            technologiesImages[i].sprite = undefined;
            results[i].sprite = undefined;
        }        

        EnableDisableDialog(false);
    }


}
