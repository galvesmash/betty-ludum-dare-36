﻿using UnityEngine;
using System.Collections;

public class CameraFollow : MonoBehaviour {

	public Transform target;

	public float speed = 1.0F;

	private float startTime;
	private float journeyLength;

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		if (transform.position != target.position) {
			journeyLength = Vector3.Distance (transform.position, target.position);
			float fracJourney = Time.deltaTime * speed / journeyLength;

			Vector3 newPosition = Vector3.Lerp (transform.position, new Vector3 (target.position.x, target.position.y, -10), fracJourney);

			if (newPosition.x > -25.6 && newPosition.x < 24.9 && newPosition.y < 26.4 && newPosition.y > -27.2) {

				transform.position = newPosition;
			} else {
				if (newPosition.x <= -25.6 || newPosition.x >= 24.9) {

					newPosition.x = transform.position.x;

				}
				if (newPosition.y >= 26.4 || newPosition.y <= -27.2) {
					newPosition.y = transform.position.y;
				}
				transform.position = newPosition;

			}

		}
	}
}
