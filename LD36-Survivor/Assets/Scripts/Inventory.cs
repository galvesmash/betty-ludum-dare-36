﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class Inventory : MonoBehaviour {

	public bool[] items = new bool[6];

	public Image[] inventoryItemImages = new Image[6];
	public Image[] requiredItemImages = new Image[3];

    public Item[] itemsCaught;
	public List <int> techsCaught = new List<int>();

	public Sprite fireSprite;
	public Sprite weelSprite;
	public Sprite coffinSprite;
	public Sprite compassSprite;
	public Sprite sailSprite;
	public Sprite goldSprite;

	// Use this for initialization
	void Start () {
		removeAllItems ();
		setRequiredItemImages ();

		for (int i = 0; i < inventoryItemImages.Length; i++) {
			inventoryItemImages [i] = GameManager.Instance.mainCanvas.inventoryPanel.transform.GetChild (0).GetChild (i).GetComponent<Image> ();
		}

		for (int i = 0; i < requiredItemImages.Length; i++) {
			requiredItemImages [i] = GameManager.Instance.mainCanvas.inventoryPanel.transform.GetChild (1).GetChild (i).GetComponent<Image> ();
		}
    }

	public void updateTechHUD(int techType){

       /* int[] requiredItems = GameManager.Instance.getRequiredItems();
        for (int i = 0; i < requiredItems.Length; i++)
        {
            if (requiredItems[i] == techType)
            {
                requiredItemImages[i].color = Color.white;
                return;
            }
        }*/
        		
		switch (techType) {
		case 0:
			techsCaught [0] = 0;
			requiredItemImages [0].sprite = fireSprite;
			requiredItemImages [0].color = Color.white;
			break;
		case 1:
			techsCaught [0] = 1;
			requiredItemImages [0].sprite = weelSprite;
			requiredItemImages [0].color = Color.white;
			break;	
		case 2:
			techsCaught [1] = 2;
			requiredItemImages [1].sprite = coffinSprite;
			requiredItemImages [1].color = Color.white;
			break;
		case 3:
			techsCaught [1] = 3;
			requiredItemImages [1].sprite = compassSprite;
			requiredItemImages [1].color = Color.white;
			break;
		case 4:
			techsCaught [2] = 4;
			requiredItemImages [2].sprite = sailSprite;
			requiredItemImages [2].color = Color.white;
			break;
		case 5:
			techsCaught [2] = 5;
			requiredItemImages [2].sprite = goldSprite;
			requiredItemImages [2].color = Color.white;
			break;
		}

	}

	void setRequiredItemImages(){
//		requiredItems = GameManager.Instance.getRequiredItems ();

		for (int i = 0; i < 3; i++) {
			techsCaught.Add (-1);

			switch (GameManager.Instance.getRequiredItems ()[i])
            {
			case 0:
				requiredItemImages [i].sprite = fireSprite;
				break;
			case 1:
				requiredItemImages [i].sprite = weelSprite;
				break;	
			case 2:
				requiredItemImages [i].sprite = coffinSprite;
				break;
			case 3:
				requiredItemImages [i].sprite = compassSprite;
				break;
			case 4:
				requiredItemImages [i].sprite = sailSprite;
				break;
			case 5:
				requiredItemImages [i].sprite = goldSprite;
				break;
			}

			requiredItemImages [i].color = Color.black;
		}
	}

	void removeAllItems(){

		for (int i = 0; i < items.Length; i++) {

			items [i] = false;
			inventoryItemImages [i].color = new Color32 (0, 0, 0, 255);

		}
	}

	public void addItem(int itemID){
		if (itemID > 0 && items [itemID - 1] == false) {
			items [itemID - 1] = true;
			inventoryItemImages [itemID - 1].color = new Color32 (255, 255, 255, 255);
		}
		Debug.Log ("Item added with ID: " + itemID);
	}

	public void removeItem(int itemID){
		items [itemID - 1] = false;
		inventoryItemImages [itemID - 1].color = new Color32 (0, 0, 0, 255);
		Debug.Log ("Item ID " + itemID + " removed");
	}

	public bool getInventoryItem(int itemID){
		return items [itemID - 1];
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
