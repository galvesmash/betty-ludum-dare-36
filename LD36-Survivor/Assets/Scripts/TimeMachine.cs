﻿using UnityEngine;
using System.Collections;

public class TimeMachine : MonoBehaviour {

	public static TimeMachine Instance;

	BoxCollider2D collider2D;

	public enum MACHINE_STATUS
	{
		IDLE,
		TALKING
	}
	public MACHINE_STATUS machineStatus;

//	public Animator uiAnimator { get; set; }

	bool waitingInteraction;

	void Awake(){
		Instance = this;
	}

	void Start () {
		collider2D = GetComponent<BoxCollider2D> ();
//		uiAnimator = GameManager.Instance.mainCanvas.machinePortrait.GetComponent<Animator> ();

		machineStatus = MACHINE_STATUS.IDLE;

		waitingInteraction = false;
	}
	
	void Update () {
		if (!GameManager.Instance.introGame) {
			if ((Input.GetKeyDown (KeyCode.E) || Input.GetKeyDown (KeyCode.Space)) && machineStatus == MACHINE_STATUS.TALKING) {
                if (GameManager.Instance.playerWin)
                {
                    GameManager.Instance.PlayerWinTheGame();
                }
				else if (!GameManager.Instance.mainCanvas.machineHudPanel.activeSelf) {
					PlayerManager.Instance.playerMovement.canMove = false;
					PlayerManager.Instance.playerMovement.freezePlayer ();
					GameManager.Instance.mainCanvas.showMachineHudPanel ();
				} else {
					PlayerManager.Instance.playerMovement.canMove = true;
					PlayerManager.Instance.playerMovement.unfreezePlayer ();
					if (machineStatus != MACHINE_STATUS.TALKING)
						GameManager.Instance.mainCanvas.showInventoryPanel ();
					else {
						GameManager.Instance.mainCanvas.showDialogPanel ();
						GameManager.Instance.mainCanvas.dialogText.text = DialogManager.Instance.getRandomText ();
					}
				}
			}

			//Entrega itens para a TimeMachine para ela ser concertada
			if (Input.GetKeyDown (KeyCode.F) && machineStatus == MACHINE_STATUS.TALKING) {
				if (!GameManager.Instance.mainCanvas.machineHudPanel.activeSelf) {                        
					int[] items = GameManager.Instance.getRequiredItems ();
					bool itemsAreOK = true;

					for (int i = 0; i < GameManager.Instance.inventory.techsCaught.Count; i++) {
						if (GameManager.Instance.inventory.techsCaught [i] == items [i]) {
							if (i == 0) {
								GameManager.Instance.mainCanvas.machineStoneTechBG.gameObject.SetActive (false);
								GameManager.Instance.mainCanvas.machineStoneTech.gameObject.SetActive (true);
								GameManager.Instance.mainCanvas.machineStoneTech.sprite = GameManager.Instance.inventory.requiredItemImages [i].sprite;

								Debug.Log ("valor i: " + i.ToString ());
							} else if (i == 1) {
								GameManager.Instance.mainCanvas.machineEgiptTechBG.gameObject.SetActive (false);
								GameManager.Instance.mainCanvas.machineEgiptTech.gameObject.SetActive (true);
								GameManager.Instance.mainCanvas.machineEgiptTech.sprite = GameManager.Instance.inventory.requiredItemImages [i].sprite;
								Debug.Log ("valor i: " + i.ToString ());
							} else if (i == 2) {
								GameManager.Instance.mainCanvas.machineMedievilTechBG.gameObject.SetActive (false);
								GameManager.Instance.mainCanvas.machineMedievilTech.gameObject.SetActive (true);
								GameManager.Instance.mainCanvas.machineMedievilTech.sprite = GameManager.Instance.inventory.requiredItemImages [i].sprite;
								Debug.Log ("valor i: " + i.ToString ());
							}
						} else
							itemsAreOK = false;
					}

					string text = "You failed with me... AGAIN!!! I don't want to see you today HUMPFF!";

					if (itemsAreOK) {
                        PlayerManager.Instance.playerMovement.canMove = false;
                        PlayerManager.Instance.playerMovement.freezePlayer();

                        text = "<3 You fixed me! You just did your obligation...";

                        GameManager.Instance.setPlayerWin();                       
					}
               
					GameManager.Instance.mainCanvas.showDialogPanel ();
					GameManager.Instance.mainCanvas.dialogText.text = text;                
				} else {
					PlayerManager.Instance.playerMovement.canMove = true;
					PlayerManager.Instance.playerMovement.unfreezePlayer (); 
                
					/*if (GameManager.Instance.gameOver) {
						GameManager.Instance.mainCanvas.showDialogPanel ();
						GameManager.Instance.mainCanvas.dialogText.text = "Let's go back to our Time, finally!";
					}  */ 
				}
			}
		}
    }

	void OnTriggerEnter2D (Collider2D coll) {
		if (!GameManager.Instance.introGame) {
			if (coll.gameObject.tag == "Player") {
				machineStatus = MACHINE_STATUS.TALKING;
				GameManager.Instance.mainCanvas.showDialogPanel ();
				GameManager.Instance.mainCanvas.dialogText.text = DialogManager.Instance.getRandomText ();
			}
		}
	}

	void OnTriggerExit2D (Collider2D coll) {
		if (!GameManager.Instance.introGame) {
			if (coll.gameObject.tag == "Player") {
				GameManager.Instance.mainCanvas.showInventoryPanel ();
				machineStatus = MACHINE_STATUS.IDLE;
			}
		}
	}

//	public void playTalkingAnim(){
//		uiAnimator.Play ("uiMachine_Talking");
//	}
//
//	public void playIdleAnim(){
//		uiAnimator.Play ("uiMachine_Idle");
//	}

}
