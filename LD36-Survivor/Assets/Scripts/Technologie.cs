﻿using UnityEngine;
using System.Collections;

public class Technologie : MonoBehaviour
{
	public enum REQUEST_TYPE: int{

		FIRE = 0,
		WEEL = 1,
		COFFIN = 2,
		COMPASS = 3,
		SAIL = 4,
		GOLD = 5
	}
    public REQUEST_TYPE technologie;

	public enum BUILT_STATUS
	{
		MISSING_ITENS = 0,
		CAN_BE_DONE = 1
	}
	public BUILT_STATUS builtItens;

	public Item.OBJECT_TYPE[] itens;

	void Start () {
		changeBuiltItens (false);
	}

	public void changeBuiltItens (bool status){
		if (status)
			builtItens = BUILT_STATUS.CAN_BE_DONE;
		else
			builtItens = BUILT_STATUS.MISSING_ITENS;
	}
}


