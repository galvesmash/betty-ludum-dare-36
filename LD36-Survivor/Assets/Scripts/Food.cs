﻿using UnityEngine;
using System.Collections;

public class Food : MonoBehaviour {

	SpriteRenderer spriteRenderer;

	public enum FOOD_STATUS {
		FULL,
		INTERACTABLE,
		EMPTY
	}
	public FOOD_STATUS foodStatus;

	public float foodValue;

	public Sprite fullSprite;
	public Sprite glowSprite;
	public Sprite emptySprite;

	GameObject glow;

	// Use this for initialization
	void Start () {
		spriteRenderer = GetComponent<SpriteRenderer> ();
		
//		if (transform.childCount > 0)
//			glow = transform.GetChild (0).gameObject;

		setFoodStatus (true);
//		glow.SetActive (false);
	}
	
	// Update is called once per frame
	void Update () {
		if ((Input.GetKeyDown (KeyCode.E) || Input.GetKeyDown (KeyCode.Space)) && foodStatus == FOOD_STATUS.INTERACTABLE) {
			setFoodStatus (false);
			PlayerManager.Instance.changeHunger (foodValue);
		}
	}

	public void setFoodStatus (bool status){
		if (status) {
			foodStatus = FOOD_STATUS.FULL;
			spriteRenderer.sprite = fullSprite;
		} else {
			foodStatus = FOOD_STATUS.EMPTY;
			spriteRenderer.sprite = emptySprite;
//			glow.SetActive (false);
		}
	}

	void OnTriggerEnter2D (Collider2D coll) {
		if (coll.gameObject.tag == "Player" && foodStatus == FOOD_STATUS.FULL) {
			foodStatus = FOOD_STATUS.INTERACTABLE;
			spriteRenderer.sprite = glowSprite;
//			glow.SetActive (true);
		}
	}

	void OnTriggerExit2D (Collider2D coll) {
		if (coll.gameObject.tag == "Player" && foodStatus == FOOD_STATUS.INTERACTABLE) {
			foodStatus = FOOD_STATUS.FULL;
			spriteRenderer.sprite = fullSprite;
//			glow.SetActive (false);
		}
	}

}
