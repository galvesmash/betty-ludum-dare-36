﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class NPCAI : MonoBehaviour {

    public enum INTERACTION_LEVEL
    {
		CANT_TALK = 0,
		NEED_A_SOLUTION = 1,
        WAITING_OBJECTS = 2,
        PROBLEM_SOLVED = 3
    }

    public WorldManager.AGES age;
    public Technologie[] technologies;
    public Dialog dialog;
    public NPCTrigger triggerDialog;
    public NPCTrigger triggerInput;

    private INTERACTION_LEVEL interactionNow;
    private bool canChange;

    private int indexProblem;

    // Use this for initialization
    void Start () {
        interactionNow = INTERACTION_LEVEL.NEED_A_SOLUTION;        

        dialog.ResetImages();

        indexProblem = 0;
    }
	
	// Update is called once per frame
	void Update () {

		if (Input.GetKeyDown(KeyCode.Space) || Input.GetKeyDown(KeyCode.E))
        {
            canChange = triggerInput.characterIsNear;

            if (canChange)
            {
                int intAge = (int)age;

                print (GameManager.Instance.inventory.techsCaught [intAge] + " == " + GameManager.Instance.getRequiredItems () [intAge]);
				if (interactionNow == INTERACTION_LEVEL.CANT_TALK
				    && GameManager.Instance.inventory.techsCaught [intAge] == GameManager.Instance.getRequiredItems () [intAge]) {
					interactionNow = INTERACTION_LEVEL.WAITING_OBJECTS;
				}
				if (interactionNow == INTERACTION_LEVEL.NEED_A_SOLUTION)
                    interactionNow = INTERACTION_LEVEL.WAITING_OBJECTS;
            }
        }

        if (triggerDialog.characterIsNear)
        {
            if (canChange)
            {
                Debug.Log(gameObject.name);

                switch (interactionNow)
                {
                case INTERACTION_LEVEL.NEED_A_SOLUTION:
                    int[] techs = { (int)technologies[0].technologie, (int)technologies[1].technologie };

                    dialog.SetImageTechnologie(techs);
                    dialog.EnableTechnologie();
                    dialog.EnableDisableDialog(true);                                              

                    break;

				case INTERACTION_LEVEL.WAITING_OBJECTS:

					refreshDialogue ();

					if (GameManager.Instance.inventory.items [((int)technologies [0].itens [0])-1]
						|| GameManager.Instance.inventory.items [((int)technologies [0].itens [1])-1])
						technologies [0].changeBuiltItens (true);
					else
						technologies [0].changeBuiltItens (false);

					if (GameManager.Instance.inventory.items [((int)technologies [1].itens [0])-1]
						|| GameManager.Instance.inventory.items [((int)technologies [1].itens [1])-1])
						technologies [1].changeBuiltItens (true);
					else
						technologies [1].changeBuiltItens (false);

                    dialog.EnableDisableDialog(true);
                    dialog.EnableItensTechnologie();
                    break;

				case INTERACTION_LEVEL.PROBLEM_SOLVED:
					indexProblem++;
					interactionNow = INTERACTION_LEVEL.CANT_TALK;

					dialog.EnableTechnologie ();
//					dialog.EnableDisableDialog (false);
                    dialog.gameObject.SetActive(false);
                    break;
                }

                canChange = false;
            }

			//Trocar objetos por tecnologia
			if (interactionNow == INTERACTION_LEVEL.WAITING_OBJECTS) {

                int buildTech = -1;

                if (Input.GetKeyDown(KeyCode.F))
                    buildTech = 0;
                else if (Input.GetKeyDown(KeyCode.G))
                    buildTech = 1;

                if (buildTech >= 0)
                {
                    if (technologies[buildTech].builtItens == Technologie.BUILT_STATUS.CAN_BE_DONE)
                    {
                        bool itemInvetory1 = false;
                        bool itemInvetory2 = false;

                        for (int f = 0; f < GameManager.Instance.inventory.items.Length; f++)
                        {
                            if (GameManager.Instance.inventory.items[f] == true && f + 1 == (int)technologies[buildTech].itens[0])
                                itemInvetory1 = true;
                            else if (GameManager.Instance.inventory.items[f] == true && f + 1 == (int)technologies[buildTech].itens[1])
                                itemInvetory2 = true;
                        }

                        if (itemInvetory1 && itemInvetory2)
                        {
                            technologies[buildTech].changeBuiltItens(false);
                            dialog.item11Status = false;
                            dialog.item12Status = false;

                            GameManager.Instance.inventory.removeItem((int)technologies[buildTech].itens[0]);
                            GameManager.Instance.inventory.removeItem((int)technologies[buildTech].itens[1]);

                            int intTech = (int)technologies[buildTech].technologie;
                            GameManager.Instance.inventory.updateTechHUD(intTech);

                            int intAge = (int)age;
                            GameManager.Instance.inventory.techsCaught[intAge] = intTech;
                        }

                        refreshDialogue();
                    }
                }            
			}

        }
        else
        {
//			dialog.EnableTechnologie ();
            dialog.EnableDisableDialog(false);
            canChange = true;
        }
    }

	void refreshDialogue(){
		for (int j = 0; j < technologies.Length; j++) {
			refreshItem (1, j);
			refreshItem (2, j);
		}
	}

	void refreshItem (int i, int j){
		if (GameManager.Instance.inventory.getInventoryItem ((int)technologies [j].itens [i-1])) {
			dialog.SetImageItem (i, j, (int)technologies [j].itens [i-1], true);
			print (i + ": true");
		} else {
			dialog.SetImageItem (i, j, (int)technologies [j].itens [i-1], false);
			print (i + ": false");
		}
	}

}
