﻿using UnityEngine;
using System.Collections;

public class PlayerMovement : MonoBehaviour {

	public SpriteRenderer spriteRenderer { get; set; }
	public bool canMove { get; set; }

	public float speed; 
	private Vector2 velocity;

	float inputX = 0;
	float inputY = 0;

	Rigidbody2D rb2d;
	RigidbodyConstraints2D constrainsRb2d;
	Animator anim;

	void Start(){
		rb2d = GetComponent<Rigidbody2D> ();
		anim = GetComponent<Animator> ();

		constrainsRb2d = rb2d.constraints;
		spriteRenderer = transform.GetChild (0).GetComponent<SpriteRenderer> ();

		canMove = true;
		anim.SetBool ("Walking", false);
		anim.SetInteger ("Direction", 0);
		anim.SetBool ("isDown", true);
	}

	void Update () {
		if (canMove && !GameManager.Instance.introGame) {
			inputX = Input.GetAxis ("Horizontal");
			inputY = Input.GetAxis ("Vertical");

			velocity = new Vector2 (speed * inputX, speed * inputY);

			if (inputX > 0) {
				anim.SetInteger ("Direction", 2);
				anim.SetBool ("Walking", true);
			} else if (inputX < 0) {
				anim.SetInteger ("Direction", 0);
				anim.SetBool ("Walking", true);
			}
			if (inputY > 0) {
				anim.SetInteger ("Direction", 1);
				anim.SetBool ("isDown", false);
				anim.SetBool ("Walking", true);
			} else if (inputY < 0) {
				anim.SetInteger ("Direction", 3);
				anim.SetBool ("isDown", true);
				anim.SetBool ("Walking", true);
			}
			
			if (inputX == 0 && inputY == 0)
				anim.SetBool ("Walking", false);

			if (velocity.x > 0 && spriteRenderer.flipX) {
				spriteRenderer.flipX = false;
			} else if (velocity.x < 0 && !spriteRenderer.flipX) {
				spriteRenderer.flipX = true;
			}
		} 
	}

	void FixedUpdate() {
		rb2d.velocity = velocity;
	}

	public void freezePlayer(){
		rb2d.constraints = RigidbodyConstraints2D.FreezeAll;
	}

	public void unfreezePlayer(){
//		rb2d.constraints = RigidbodyConstraints2D.None;
		rb2d.constraints = RigidbodyConstraints2D.FreezeRotation;
	}

}
