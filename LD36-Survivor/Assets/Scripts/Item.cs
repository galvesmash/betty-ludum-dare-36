﻿using UnityEngine;
using System.Collections;

public class Item : MonoBehaviour {

	SpriteRenderer spriteRederer;
	CircleCollider2D collider2D;

	public Sprite fullSprite;
	public Sprite glowSprite;
	public Sprite emptySprite;

	public enum ITEM_STATUS{
		IDLE,
		GLOW,
		EMPTY,
		HUD
	}
	public ITEM_STATUS itemStatus;
	
	public enum OBJECT_TYPE 
	{		
		UNDEFINED = 0,
		WOOD = 1,
		LEATHER = 2,
		LENTS = 3,
		POTION = 4,
		SWORD = 5,
		KNOWLEDG = 6
	}

	public OBJECT_TYPE itemType { get; set; }
	public int itemID;

//	public float distanceFromPlayer;

	void Start () {
		spriteRederer = GetComponent<SpriteRenderer> ();
		if (GetComponent<CircleCollider2D> () != null)
			collider2D = GetComponent<CircleCollider2D> ();

		switch (itemID) {
		case 1:
			itemType = OBJECT_TYPE.WOOD;
			break;
		case 2:
			itemType = OBJECT_TYPE.LEATHER;
			break;
		case 3:
			itemType = OBJECT_TYPE.LENTS;
			break;
		case 4:
			itemType = OBJECT_TYPE.POTION;
			break;
		case 5:
			itemType = OBJECT_TYPE.SWORD;
			break;
		case 6:
			itemType = OBJECT_TYPE.KNOWLEDG;
			break;
		default:
			itemType = OBJECT_TYPE.UNDEFINED;
			break;
		}

		if (itemStatus == ITEM_STATUS.HUD)
			collider2D.enabled = false;

		spriteRederer.sprite = fullSprite;
//		spriteRederer.sprite = Resources.Load<Sprite> ("Itens/item" + itemID);
//		glow.gameObject.GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite> ("Itens/item" + itemID);
	}
	
	void Update () {
		if ((Input.GetKeyDown (KeyCode.E) || Input.GetKeyDown (KeyCode.Space)) && itemStatus == ITEM_STATUS.GLOW) {
			collectItem ();
		}
	}

	void OnTriggerEnter2D (Collider2D coll) {
		if (coll.gameObject.tag == "Player" && itemStatus != ITEM_STATUS.EMPTY) {
			itemStatus = ITEM_STATUS.GLOW;
			spriteRederer.sprite = glowSprite;
		}
	}

	void OnTriggerExit2D (Collider2D coll) {
		if (coll.gameObject.tag == "Player" && itemStatus != ITEM_STATUS.EMPTY) {
			itemStatus = ITEM_STATUS.IDLE;
			spriteRederer.sprite = fullSprite;
		}
	}

	void collectItem(){
        Inventory inventory = GameObject.Find("GameManager").GetComponent<Inventory>();

        if (!inventory.getInventoryItem(itemID))
        {
            print("Coletando " + name);
			inventory.addItem(itemID);

//			if (emptySprite != null){
//				itemStatus = ITEM_STATUS.EMPTY;
//				spriteRederer.sprite = emptySprite;
//			}
//			else
        	Destroy(gameObject);
        }
	}

}
