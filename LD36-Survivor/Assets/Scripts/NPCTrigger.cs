﻿using UnityEngine;
using System.Collections;

public class NPCTrigger : MonoBehaviour {

    public bool characterIsNear;
//    public Inventory playerInventory;

    // Use this for initialization
    void Start ()
    {
//		playerInventory = other.gameObject.GetComponent<Inventory>();
//		playerInventory = PlayerManager.Instance.inventory;
        characterIsNear = false;
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Player")
        {
            Debug.Log("Player entrou da área do NPC");
//            playerInventory = other.gameObject.GetComponent<Inventory>();
            characterIsNear = true;
        }
    }

    void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.tag == "Player")
        {
            Debug.Log("Player saiu da área do NPC");
            characterIsNear = false;            
        }
    }
}
