﻿using UnityEngine;
using System.Collections;

public class SpawnManager: MonoBehaviour {

	//To easly change the number of itens, create a variable to determine it. heeeeeell yeeeeeah

	int[] spawners = new int[12];
	int[] spawners1 = new int[4];
	int[] spawners2 = new int[4];
	int[] spawners3 = new int[4];

	int[] monumentSpawners = new int[3];


	int[] foodSpawners1 = new int[2];
	int[] foodSpawners2 = new int[2];
	int[] foodSpawners3 = new int[2];
	int[] foodSpawners = new int[6];

	ArrayList itensToGenerate1 = new ArrayList();
	ArrayList itensToGenerate2 = new ArrayList();
	ArrayList itensToGenerate3 = new ArrayList();


	GameObject[] objectsCreated = new GameObject[12];
	GameObject[] monumentsCreated = new GameObject[3];
	GameObject[] foodsCreated = new GameObject[3];
	GameObject[] npcCreated = new GameObject[3];
	string[] prefabs = new string[12];


	string[] monumentPrefabs = new string[3];
	string[] foodPrefabs = new string[6];
	string[] npcPrefab = new string[3];
 
	int createInSpawner1 = 1;
	int createInSpawner2 = 2;
	int createInSpawner3 = 3;
	int createInSpawner4 = 4;
	int createInSpawner5 = 5;
	int createInSpawner6 = 6;
	int createInSpawner7 = 7;
	int createInSpawner8 = 8;
	int createInSpawner9 = 9;
	int createInSpawner10 = 10;
	int createInSpawner11 = 11;
	int createInSpawner12 = 12;

	int monumentSpawner1;
	int monumentSpawner2;
	int monumentSpawner3;

	int foodInSpawner1;
	int foodInSpawner2;
	int foodInSpawner3;

	// Use this for initialization
	void Start () {
		/*

		prefabs [0] = "Prefabs/Itens/Wood";
		prefabs [1] = "Prefabs/Itens/Leather";
		prefabs [2] = "Prefabs/Itens/Lents"; 
		prefabs [3] = "Prefabs/Itens/Potion";
		prefabs [4] = "Prefabs/Itens/Sword";
		prefabs [5] = "Prefabs/Itens/Knowledge";
		*/

		//*

		prefabs [0] = "Prefabs/Itens/Wood";
		prefabs [1] = "Prefabs/Itens/Wood";
		prefabs [2] = "Prefabs/Itens/Leather";
		prefabs [3] = "Prefabs/Itens/Leather";
		prefabs [4] = "Prefabs/Itens/Lents";
		prefabs [5] = "Prefabs/Itens/Lents";
		prefabs [6] = "Prefabs/Itens/Potion";
		prefabs [7] = "Prefabs/Itens/Potion";
		prefabs [8] = "Prefabs/Itens/Sword"; 
		prefabs [9] = "Prefabs/Itens/Sword";
		prefabs [10] = "Prefabs/Itens/Knowledge";
		prefabs [11] = "Prefabs/Itens/Knowledge";
		//*/

		monumentPrefabs [0] = "Prefabs/Monuments/cave";
		monumentPrefabs [1] = "Prefabs/Monuments/pyramid";
		monumentPrefabs [2] = "Prefabs/Monuments/castle";

		foodPrefabs [0] = "Prefabs/Foods/Food1";
		foodPrefabs [1] = "Prefabs/Foods/Food1";
		foodPrefabs [2] = "Prefabs/Foods/Food2";
		foodPrefabs [3] = "Prefabs/Foods/Food2";
		foodPrefabs [4] = "Prefabs/Foods/Food3";
		foodPrefabs [5] = "Prefabs/Foods/Food3";

		npcPrefab [0] = "Prefabs/NPCs/NPC_Stone";
		npcPrefab [1] = "Prefabs/NPCs/NPC_Egipt";
		npcPrefab [2] = "Prefabs/NPCs/NPC_Medievil";

		this.randomizeItens ();
		this.randomizeMonuments ();

		this.setItemSpawnPoints ();
		this.setMonumentPoints ();
		this.setFoodSpawnPoint ();

		this.generateNPCs ();
	}

	void randomizeMonuments(){

		monumentSpawner1 = Random.Range (1, 3);
		monumentSpawner2 = Random.Range (3, 5);
		monumentSpawner3 = Random.Range (5, 7);

		monumentSpawners [0] = monumentSpawner1;
		monumentSpawners [1] = monumentSpawner2;
		monumentSpawners [2] = monumentSpawner3;


	}
	void randomizeItens(){


		for (int i = 1; i < 8; i++) {
			itensToGenerate1.Add(i);
		}		
		for (int i = 8; i < 15; i++) {
			itensToGenerate2.Add(i);
		}		
		for (int i = 15; i < 22; i++) {
			itensToGenerate3.Add(i);
		}

		for (int i = 0; i < spawners1.Length; i++) {
			int thisNumber = Random.Range(0, itensToGenerate1.Count);
			spawners1[i] = (int) itensToGenerate1[thisNumber];
			itensToGenerate1.RemoveAt(thisNumber);
		}		
		for (int i = 0; i < spawners2.Length; i++) {
			int thisNumber = Random.Range(0, itensToGenerate1.Count);
			spawners2[i] = (int) itensToGenerate2[thisNumber];
			itensToGenerate2.RemoveAt(thisNumber);
		}	
		for (int i = 0; i < spawners3.Length; i++) {
			int thisNumber = Random.Range(0, itensToGenerate1.Count);
			spawners3[i] = (int) itensToGenerate3[thisNumber];
			itensToGenerate3.RemoveAt(thisNumber);
		}

		//spawners1 = this.reshuffle (spawners1);
		//spawners2 = this.reshuffle (spawners2);
		//spawners3 = this.reshuffle (spawners3);


		spawners [0] = spawners1 [0];
		spawners [1] = spawners1 [1];
		spawners [2] = spawners1 [2];
		spawners [3] = spawners1 [3];
		spawners [4] = spawners2 [0];
		spawners [5] = spawners2 [1];
		spawners [6] = spawners2 [2];
		spawners [7] = spawners2 [3];
		spawners [8] = spawners3 [0];
		spawners [9] = spawners3 [1];
		spawners [10] = spawners3 [2];
		spawners [11] = spawners3 [3];



		for (int i = 0; i < foodSpawners1.Length; i++) {
			int thisNumber = Random.Range(0, itensToGenerate1.Count);
			foodSpawners1[i] = (int) itensToGenerate1[thisNumber];
			itensToGenerate1.RemoveAt(thisNumber);
		}		
		for (int i = 0; i < foodSpawners2.Length; i++) {
			int thisNumber = Random.Range(0, itensToGenerate1.Count);
			foodSpawners2[i] = (int) itensToGenerate2[thisNumber];
			itensToGenerate2.RemoveAt(thisNumber);
		}	
		for (int i = 0; i < foodSpawners3.Length; i++) {
			int thisNumber = Random.Range(0, itensToGenerate1.Count);
			foodSpawners3[i] = (int) itensToGenerate3[thisNumber];
			itensToGenerate3.RemoveAt(thisNumber);
		}

		foodSpawners [0] = foodSpawners1 [0];
		foodSpawners [1] = foodSpawners1 [1];
		foodSpawners [2] = foodSpawners2 [0];
		foodSpawners [3] = foodSpawners2 [1];
		foodSpawners [4] = foodSpawners3 [0];
		foodSpawners [5] = foodSpawners3 [1];

		//createInSpawner1 = Random.Range (1, 6);
		//createInSpawner2 = Random.Range (1, 6);	
		//createInSpawner2 = Random.Range (1, 6);
		//createInSpawner4 = Random.Range (1, 6);

//		if (createInSpawner1 == createInSpawner2 || createInSpawner1 == createInSpawner3 || createInSpawner1 == createInSpawner4){
//			if (createInSpawner2 < 5){
//				createInSpawner2++;
//			} else {
//				createInSpawner2--;
//			}
//		}

//		foodInSpawner1 = Random.Range (1, 6);
//
//		if (foodInSpawner1 == createInSpawner1 || foodInSpawner1 == createInSpawner2) {
//
//			if (foodInSpawner1 <= 3) {
//
//				while (foodInSpawner1 == createInSpawner1 || foodInSpawner1 == createInSpawner2) {
//
//					foodInSpawner1++;
//				}
//			} else {
//				while (foodInSpawner1 == createInSpawner1 || foodInSpawner1 == createInSpawner2) {
//
//					foodInSpawner1--;
//				}
//			}
//		}
//
//		createInSpawner3 = Random.Range (6, 11);
//		createInSpawner4 = Random.Range (6, 11);
//		if (createInSpawner3 == createInSpawner4){
//			if (createInSpawner4 < 10){
//				createInSpawner4++;
//			} else {
//				createInSpawner4--;
//			}
//		}
//		foodInSpawner2 = Random.Range (6, 11);
//
//		if (foodInSpawner2 == createInSpawner3 || foodInSpawner2 == createInSpawner4) {
//
//			if (foodInSpawner2 <= 8) {
//
//				while (foodInSpawner2 == createInSpawner3 || foodInSpawner2 == createInSpawner4) {
//
//					foodInSpawner2++;
//				}
//			} else {
//				while (foodInSpawner2 == createInSpawner3 || foodInSpawner2 == createInSpawner4) {
//
//					foodInSpawner2--;
//				}
//			}
//		}
//
//		createInSpawner5 = Random.Range (11, 16);
//		createInSpawner6 = Random.Range (11, 16);
//		if (createInSpawner5 == createInSpawner6){
//			if (createInSpawner6 < 15){
//				createInSpawner6++;
//			} else {
//				createInSpawner6--;
//			}
//		}
//
//		foodInSpawner3 = Random.Range (11, 16);
//
//		if (foodInSpawner3 == createInSpawner5 || foodInSpawner3 == createInSpawner6) {
//
//			if (foodInSpawner3 <= 13) {
//
//				while (foodInSpawner3 == createInSpawner5 || foodInSpawner3 == createInSpawner6) {
//
//					foodInSpawner3++;
//				}
//			} else {
//				while (foodInSpawner3 == createInSpawner5 || foodInSpawner3 == createInSpawner6) {
//
//					foodInSpawner3--;
//				}
//			}
//		}

//		spawners [0] = createInSpawner1;
//		spawners [1] = createInSpawner2;
//		spawners [2] = createInSpawner3;
//		spawners [3] = createInSpawner4;
//		spawners [4] = createInSpawner5;
//		spawners [5] = createInSpawner6;

//		foodSpawners [0] = foodInSpawner1;
//		foodSpawners [1] = foodInSpawner2;
//		foodSpawners [2] = foodInSpawner3;

	}

	int[] reshuffle(int[] texts)
	{
		// Knuth shuffle algorithm :: courtesy of Wikipedia :)
		for (int t = 0; t < texts.Length; t++ )
		{
			int tmp = texts[t];
			int r = Random.Range(t, texts.Length);
			texts[t] = texts[r];
			texts[r] = tmp;
		}
		return texts;
	}


	void setMonumentPoints(){

		for (int i = 0; i < 3; i++) {
			monumentsCreated[i] = (GameObject)Instantiate (Resources.Load (monumentPrefabs[i]));
			monumentsCreated[i].transform.parent = GameObject.Find ("MonumentSpawn" + monumentSpawners[i].ToString()).transform;
			monumentsCreated[i].transform.position = monumentsCreated[i].transform.parent.transform.position;

		}
	}

	void setItemSpawnPoints(){

		int aux = 0;
		for (int i = 0; i < 12; i++) {
			
			objectsCreated[i] = (GameObject)Instantiate (Resources.Load (prefabs[i]));
			objectsCreated[i].transform.parent = GameObject.Find ("ItemSpawn" + spawners[i].ToString()).transform;
			objectsCreated[i].transform.position = objectsCreated[i].transform.parent.transform.position;
			if (i % 2 == 0) {
				aux++;
			}
		}

	}

	void setFoodSpawnPoint(){

		for (int i = 0; i < 3; i++) {
			foodsCreated[i] = (GameObject)Instantiate (Resources.Load (foodPrefabs[i]));
			foodsCreated[i].transform.parent = GameObject.Find ("ItemSpawn" + foodSpawners[i].ToString()).transform;
			foodsCreated[i].transform.position = foodsCreated[i].transform.parent.transform.position;

		}

	}

	void generateNPCs(){

		for (int i = 0; i < 3; i++) {

			npcCreated [i] = (GameObject)Instantiate (Resources.Load (npcPrefab [i]));
			npcCreated [i].transform.parent = monumentsCreated [i].transform;
			npcCreated[i].transform.localPosition = new Vector3 (10, - 10, npcCreated[i].transform.parent.transform.position.z);
			//npcCreated [i].transform.position = new Vector3 (10, - 10, npcCreated[i].transform.parent.transform.position.z);
			//	npcCreated [i].transform.position.y = npcCreated [i].transform.parent.position.y - 10;
		}
	}

	// Update is called once per frame
	void Update () {
	
	}
}
