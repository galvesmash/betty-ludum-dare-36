﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerManager : MonoBehaviour {

	public static PlayerManager Instance;

	public PlayerMovement playerMovement { get; set; }

	public int maxHP, maxHunger;
	public float hp { get; set; }

	public float hunger { get; set; }

	public float hpSpeed;
	public float hungerSpeed; //0.01

	public Inventory inventory { get; set; }

//	List<Item> itens = new List<Item> ();

	void Awake() {
		Instance = this;
	}

	void Start() {
		playerMovement = GetComponent<PlayerMovement> ();

		hunger = maxHunger;
		hp = maxHP;
//		itens.AddRange (FindObjectsOfType<Item> ());
	}
	
	void Update() {
		if (!GameManager.Instance.introGame) {
			if (hunger > 0) {
				hunger -= Mathf.Lerp (0f, maxHunger, Time.deltaTime * hungerSpeed);
			} else {
				hp -= Mathf.Lerp (0f, maxHP, Time.deltaTime * hpSpeed);
			}

			if (hp <= 0) {
				playerMovement.canMove = false;
				playerMovement.freezePlayer ();

				GameManager.Instance.GameOver ();
			}
		}
	}

	public void changeHunger(float value){
		hunger += value;
		if (hunger > maxHunger)
			hunger = maxHunger;
		else if (hunger < 0)
			hunger = 0;
	}

}
