﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MainCanvasManager : MonoBehaviour {

//	public static MainCanvasManager Instance;

	public GameObject inventoryPanel;
	public GameObject machineHudPanel;
	public GameObject dialogPanel;
	public RectTransform machinePortrait;

	public Image machineStoneTechBG;
	public Image machineEgiptTechBG;
	public Image machineMedievilTechBG;

	public Image machineStoneTech;
	public Image machineEgiptTech;
	public Image machineMedievilTech;

	public Text dialogText;

    public Image hungerBar;
    public Image hpBar;

    public Image theEnd;
    public Image gameOver;

//	void Awake(){
//		Instance = this;
//	}

	void Start () {
//		dialogText = dialogPanel.transform.GetChild (0).GetComponent<Text> ();

		inventoryPanel.SetActive (true);
		machineHudPanel.SetActive (false);
		dialogPanel.SetActive (false);

        gameOver.gameObject.SetActive(false);
        theEnd.gameObject.SetActive(false);
    }

	public void setDialogText(string text){

        if (dialogText != null)
		    dialogText.text = text;
	}

	void Update () {

        if (PlayerManager.Instance.hunger > 0)
        {
            hungerBar.fillAmount = (PlayerManager.Instance.hunger * 100 / PlayerManager.Instance.maxHunger) / 100;
        }
        else
            hungerBar.fillAmount = 0;

        if (PlayerManager.Instance.hp > 0)
        {
            hpBar.fillAmount = (PlayerManager.Instance.hp * 100 / PlayerManager.Instance.maxHP) / 100;
        }
        else
            hpBar.fillAmount = 0;

        if (Input.GetMouseButtonDown(0))
        {
            if (GameManager.Instance.gameOver)
            {
                Application.LoadLevel("Menu");
            }
        }
    }

	public void showDialogPanel(){
		dialogPanel.SetActive (true);
		inventoryPanel.SetActive (false);
		machineHudPanel.SetActive (false);
	}

	public void showInventoryPanel(){
		inventoryPanel.SetActive (true);
		dialogPanel.SetActive (false);
		machineHudPanel.SetActive (false);
	}

	public void showMachineHudPanel(){
		machineHudPanel.SetActive (true);
		dialogPanel.SetActive (false);
		inventoryPanel.SetActive (false);
	}

    public void refreshHungerBar(float hungerMax, float hungerNow)
    {
        hungerBar.fillAmount = (hungerNow * 100 / hungerMax) / 100;
    }

    public void enableGameOver()
    {
        theEnd.gameObject.SetActive(false);
        gameOver.gameObject.SetActive(true);
    }

    public void enablTheEnd()
    {
        gameOver.gameObject.SetActive(false);
        theEnd.gameObject.SetActive(true);
    }


    //	public void hideDialogPanel(){
    //		dialogPanel.SetActive (false);
    //		inventoryPanel.SetActive (false);
    //		machineHudPanel.SetActive (true);
    //	}
}
