﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class MenuManager : MonoBehaviour {

	public Canvas StartMenu;
	public Canvas Dialog;
	public Button quit;
	
	void Start () {
		if (Dialog != null) {
			Dialog.gameObject.SetActive(false);
		}
	}

	public void PlayGame()
	{
		Application.LoadLevel("game");
	}

	public void OpenDialog()
	{
		Dialog.gameObject.SetActive(true);
	}

	public void exitGame()
	{
		Application.Quit();
	}

	public void cancelExit()
	{
		Dialog.gameObject.SetActive(false);
	}

	public void Credits()
	{
		Application.LoadLevel("Credits");
	}

	public void MainMenu()
	{
		Application.LoadLevel("Menu");
	}
}
