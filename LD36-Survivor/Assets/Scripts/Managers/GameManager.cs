﻿using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour
{
	public static GameManager Instance;

    public bool gameOver = false;
    public bool playerWin = false;

    public Inventory inventory { get; set; }

	public MainCanvasManager mainCanvas { get; set; }

	public enum REQUEST_TYPE {
		FIRE = 0,
		WEEL = 1,
		COFFIN = 2,
		COMPASS = 3,
		SAIL = 4,
		GOLD = 5
	}
    public Item.OBJECT_TYPE[] objects;

	int rockItem;
	int egiptItem;
	int medievalItem;

	public bool introGame { get; set; }

	void Awake(){
		Instance = this;
		introGame = true;
        gameOver = false;
        playerWin = false;
    }

	void Start (){
		mainCanvas = FindObjectOfType<MainCanvasManager> ();
		inventory = GetComponent<Inventory> ();

		int rockItemValue = Random.Range (0, 2);
		int egiptItemValue = Random.Range (2, 4);
		int medievalItemValue = Random.Range (4, 6);

		rockItem = rockItemValue;
		egiptItem = egiptItemValue;
		medievalItem = medievalItemValue;

		StartCoroutine (startIntro());
	}    

    IEnumerator startIntro (){
		GameManager.Instance.mainCanvas.showDialogPanel ();
		GameManager.Instance.mainCanvas.setDialogText (DialogManager.Instance.getIntroText ());

		while (DialogManager.Instance.introDialog.Count > 0) {
			if (Input.GetKeyDown(KeyCode.Space) || Input.GetKeyDown(KeyCode.E))
				GameManager.Instance.mainCanvas.setDialogText (DialogManager.Instance.getIntroText ());

			yield return 0;
		}

		GameManager.Instance.mainCanvas.showInventoryPanel ();
		introGame = false;
	}

	public int[] getRequiredItems(){

		int[] requiredItems = new int[3];
		requiredItems [0] = rockItem;
		requiredItems [1] = egiptItem;
		requiredItems [2] = medievalItem;
		return requiredItems;
	}

    public void setPlayerWin()
    {
        playerWin = true;
    }

    public void PlayerWinTheGame()
    {
        gameOver = true;

        PlayerManager.Instance.playerMovement.canMove = false;
        PlayerManager.Instance.playerMovement.freezePlayer();

        GameManager.Instance.mainCanvas.enablTheEnd();
    }

    public void GameOver()
    {
        gameOver = true;

        PlayerManager.Instance.playerMovement.canMove = false;
        PlayerManager.Instance.playerMovement.freezePlayer();

        GameManager.Instance.mainCanvas.enableGameOver();
    }
}


