﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DialogManager : MonoBehaviour {

	public static DialogManager Instance;

	public List<string> introDialog = new List <string> ();
	public List<string> gameDialog = new List <string> (); 

	float maxDialogs;

	void Awake(){
		Instance = this;

//		introDialog.Add ("...");
//		introDialog.Add ("Tim...");
//		introDialog.Add ("How could you forget THAT?! That was the best moment of our lifes...");
//		introDialog.Add ("You bastard, I HATE YOU!");
//		introDialog.Add ("Now I'm all broken! And you are stuck here, between time and space.");
//		introDialog.Add ("YOU DID IT, CONTRATULATIONS!");
	}

	void Start () {
		introDialog.Add ("Tutorial: Press \"E\" or \"SPACE\" to interact.");
		introDialog.Add ("Tutorial: Press \"WASD\" or \"Arrows\" to Move.");
		introDialog.Add ("...");
		introDialog.Add ("Betty: Tim...");
		introDialog.Add ("Betty: How could you forget THAT?! That was the best moment of our lifes...");
		introDialog.Add ("Betty: You bastard, I HATE YOU!");
		introDialog.Add ("Betty: Now I'm all broken! And you are stuck here, between time and space.");
		introDialog.Add ("Betty: YOU DID IT, CONTRATULATIONS!");
		introDialog.Add ("Betty: Now get me those 3 Ancient Techs and FIX ME!!!!");
		introDialog.Add ("...");

		gameDialog.Add ("Betty: What do you want now?");
		gameDialog.Add ("Betty: Get out of here!");
		gameDialog.Add ("Betty: Stop talking to me you bastard.");
		gameDialog.Add ("Betty: I'm not ringing for you!");

	}

	void Update () {
	
	}

	public string getRandomText(){
		int i = (int)Random.Range (0, gameDialog.Count);
		return gameDialog[i];
	}

	public string getIntroText(){
		string text = introDialog [0];
		introDialog.Remove (text);
		return text;
	}
}
